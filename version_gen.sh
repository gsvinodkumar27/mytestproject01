#!/bin/bash

# Version file path
versionFile="version.txt"

# Check out the main branch
git checkout main

git config --global user.email "gsvinodkumar27@gmail.com"
git config --global user.name "Vinod Kumar"

# Read the current version from the version file (if it exists)
if [ -f "$versionFile" ]; then
    currentVersion=$(cat "$versionFile")
else
    currentVersion="1.0.0"
fi

# Split the current version into major, minor, and patch components
IFS='.' read -ra versionComponents <<< "$currentVersion"
major=${versionComponents[0]}
minor=${versionComponents[1]}
patch=${versionComponents[2]}

# Increment the patch version by 1
newPatch=$((patch + 1))

# Generate the new version string
newVersion="$major.$minor.$newPatch"

# Update the version file with the new version using printf
printf "%s" "$newVersion" > "$versionFile"

# Output the new version
echo "New version: $newVersion"

#inside version file
echo "Indise version file"
cat $versionFile

# git tag -a -m "Update version to $VERSION" Version "$newVersion"

# Commit and push the updated version file
# git remote add origin https://gitlab.com/myfirstgroup4911131/myfirstproject.git
# git branch -M main
# git push -uf origin main
# git push origin https://gsvinodkumar27:glft-aY66XCWd9g--kedsGqsv@gitlab.com/myfirstgroup4911131/myfirstproject.git 





# git add "$versionFile"
# git commit -m "Increment version to $newVersion"
# git push origin main

# Tag the commit with the new version and push the tag
git tag "$newVersion"
git push origin "$newVersion"
